import React from "react";
import {
  TextInput,
  Create,
  SimpleForm,
  email,
  required,
  PasswordInput,
  CheckboxGroupInput,
  useNotify,
  useRedirect,
  useRefresh,
  ArrayInput,
  SimpleFormIterator,
  NumberInput,
} from "react-admin";

const CommentCreate = () => {
  const notify = useNotify();
  const redirect = useRedirect();
  const refresh = useRefresh();

  const onSuccess = () => {
    notify("Berhasil menambahkan admin baru", {
      type: "success",
    });
    redirect("/");
    refresh();
  };

  return (
    <Create mutationOptions={{ onSuccess }}>
      <SimpleForm>
        <TextInput source="name" />
        <TextInput source="email" />
        <TextInput source="body" />
      </SimpleForm>
    </Create>
  );
};

export default CommentCreate;
