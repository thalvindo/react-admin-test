import { List, Datagrid, TextField, ShowButton, TopToolbar, SelectColumnsButton, CreateButton, ExportButton } from "react-admin";

const CommentListActions = () => {
  return (
    <TopToolbar>
      <CreateButton />
      <ExportButton />
    </TopToolbar>
  );
};

const CommentList = () => (
  <List actions={<CommentListActions />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="email" />
      <TextField source="body" />
      <ShowButton label="Detail"></ShowButton>
      <CreateButton></CreateButton>
    </Datagrid>
  </List>
);

export default CommentList;
