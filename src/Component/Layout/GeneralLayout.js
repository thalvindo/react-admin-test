// in src/MyLayout.js
import { Layout } from "react-admin";
import GeneralAppBar from "../AppBar/GeneralAppBar";

import GeneralMenu from "../Menu/GeneralMenu";

const GeneralLayout = (props) => {
  return <Layout {...props} menu={GeneralMenu} appBar={GeneralAppBar} />;
};

export default GeneralLayout;
