import { SimpleShowLayout, TextField, Show } from "react-admin";

const CommentShow = () => {
  return (
    <Show>
      <SimpleShowLayout>
        <TextField source="id" />
        <TextField source="email" />
        <TextField source="body" />
      </SimpleShowLayout>
    </Show>
  );
};
export default CommentShow;
