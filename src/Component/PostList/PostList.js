import * as React from "react";
import { Fragment, useCallback } from "react";
import { useListContext, List, Datagrid, TextField, DateField, BooleanField, Show, TabbedShowLayout, RichTextField } from "react-admin";

import { useMediaQuery, Divider, Tabs, Tab, Theme } from "@mui/material";

import "./PostList.css";

const tabs = [
  { id: "input", name: "input" },
  { id: "monitoring", name: "monitoring" },
];

const PostList = () => {
  const [someState, setSomeState] = React.useState("input");
  const handleOnChange = (e) => {
    console.log(e.target.textContent);
    setSomeState(e.target.textContent);
  };
  console.log(someState);

  return (
    <>
      <h1>hahaha</h1>
      <Tabs value={"asd"} onChange={(e) => handleOnChange(e)} centered>
        {tabs.map((choice) => (
          <Tab key={choice.id} label={choice.name} value={choice.id} />
        ))}
      </Tabs>

      <Divider />
      <>{someState === "input" ? <div> this is input</div> : <div> this is monitoring</div>}</>
    </>
  );
};

export default PostList;

//   <div className="outer">
//       <List>
//         <Datagrid>
//           <TextField source="id" />
//           <TextField source="title" />
//           <BooleanField source="commentable" />
//         </Datagrid>
//       </List>
//   </div>
