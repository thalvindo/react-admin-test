import * as React from "react";
import { AppBar } from "react-admin";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import { deepOrange, deepPurple } from "@mui/material/colors";

import "./GeneralAppBar.css";

const GeneralAppBar = () => {
  return (
    <AppBar
      sx={{
        "& .RaAppBar-title": {
          flex: 1,
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
          overflow: "hidden",
          position: "relative",
        },
      }}
      className="app-bar"
    >
      <div className="welcome-area">
        <p className="welcome-text">Welcome, Siapapun~</p>
        <Avatar sx={{ bgcolor: deepOrange[500] }}>Nd</Avatar>
      </div>
    </AppBar>
  );
};

export default GeneralAppBar;
