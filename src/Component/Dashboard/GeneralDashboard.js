import React from "react";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import { deepOrange, deepPurple } from "@mui/material/colors";

import "./GeneralDashboard.css";

const GeneralDashboard = (id, data) => {
  console.log(id, data);
  return (
    <>
      <Stack className="avatar-icon">
        <Avatar sx={{ bgcolor: deepOrange[500], width: 200, height: 200 }} className="avatar">
          N
        </Avatar>
      </Stack>

      <div>
        <span>Nama</span>
        <p>Alvindo Tehmono</p>
      </div>
    </>
  );
};

export default GeneralDashboard;
