import { Menu, DashboardMenuItem } from "react-admin";
import LabelIcon from "@mui/icons-material/Label";
import { Link } from "react-router-dom";

const GeneralMenu = () => {
  return (
    <Menu>
      <Menu.DashboardItem />
      <hr />
      <Menu.ResourceItem name="posts" />
      <Menu.ResourceItem name="comments" />
      <Menu.ResourceItem name="users" />
      <Menu.Item to="/custom-route" primaryText="Miscellaneous" leftIcon={<LabelIcon />} />
    </Menu>
    // <div>
    //   <div className="top">
    //     <Link to="/" style={{ textDecoration: "none" }}>
    //       <span className="logo">Dashboard</span>
    //     </Link>
    //   </div>
    //   <hr />
    //   <div>
    //     <Link to="/posts">posts</Link>
    //     <hr />
    //     <Link to="/comments">Comments</Link>
    //     <hr />
    //     <Link to="/users">Users</Link>
    //   </div>
    // </div>
  );
};

export default GeneralMenu;
