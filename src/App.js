import React from "react";
import { Admin, Resource, ListGuesser, combineDataProviders } from "react-admin";
import jsonServerProvider from "ra-data-json-server";
import GeneralLayout from "./Component/Layout/GeneralLayout";
import GeneralDashboard from "./Component/Dashboard/GeneralDashboard";
import PostList from "./Component/PostList/PostList";
import axios from "axios";
import CommentList from "./Component/CommentList/CommentList";
import CommentShow from "./Component/CommentShow/CommentShow";
import CommentCreate from "./Component/CommentCreate/CommentCreate";

const dataProvider = jsonServerProvider("https://jsonplaceholder.typicode.com");
const postDataProvider = jsonServerProvider("https://jsonplaceholder.typicode.com");

const whateverThisIs = {
  getList: async (resource, params) => {
    console.log("res", resource);
    console.log("params", params);

    try {
      const json = await axios.get(`https://jsonplaceholder.typicode.com/${resource}`);

      console.log(json);
      return {
        data: json.data,
        total: json.data.length,
      };
    } catch (error) {
      console.log(error);
      return Promise.reject(new Error("Gagal memuat data. Mohon periksa kembali koneksi internet Anda atau hubungi developer."));
    }
  },
  getOne: async (resource, params) => {
    console.log(resource, params);
    try {
      const json = await axios.get(`https://jsonplaceholder.typicode.com/${resource}`);

      return {
        data: json.data[0],
      };
    } catch (error) {
      console.log(error);
      return Promise.reject(new Error("Gagal memuat data. Mohon periksa kembali koneksi internet Anda atau hubungi developer."));
    }
  },
  create: async (resource, params) => {
    console.log(resource, params);

    try {
      let newData = { postId: 1, name: params.data.name, email: params.data.email, body: params.data.body };
      console.log("newdata", newData);
      const json = await axios.post(`https://jsonplaceholder.typicode.com/${resource}`, newData);
      console.log("JSON BOY", json);
      if (json.status === 201) {
        return {
          data: json.data,
        };
      }
    } catch (error) {
      console.log(error);
      return Promise.reject(new Error("Gagal memuat data. Mohon periksa kembali koneksi internet Anda atau hubungi developer."));
    }
  },
};

const id = 1;
const data = 2;
const App = () => {
  const dataProvider = combineDataProviders((resource) => {
    switch (resource) {
      case "users":
        return postDataProvider;
      case "comments":
        return whateverThisIs;
      default:
        throw new Error(`Unknown resource: ${resource}`);
    }
  });

  return (
    <Admin dataProvider={dataProvider} layout={GeneralLayout} dashboard={() => GeneralDashboard(id, data)}>
      <Resource name="posts" list={PostList} />
      <Resource name="comments" list={CommentList} show={CommentShow} create={CommentCreate} />
      <Resource name="users" list={ListGuesser} />
    </Admin>
  );
};

export default App;
